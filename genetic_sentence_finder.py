import random
from string import ascii_lowercase
from math import exp


def fitness(individual):
    matching_letters = 0
    for i, letter in enumerate(individual):
        if letter == target_sentence[i]:
            matching_letters += 1
    return exp(matching_letters)


def cross_over(parent_a, parent_b):
    midpoint = len(target_sentence) // 2
    return parent_a[:midpoint] + parent_b[midpoint:]


def mutate(individual):
    individual[random.randrange(len(individual))] = random.choice(letters)
    return individual


target_sentence = "i love genetic algorithms"
letters = list(ascii_lowercase + " ")
population_size = 100
population = [random.choices(letters, k=len(target_sentence)) for x in range(population_size)]
iterations_count = 0
mutation_rate = 0.8
best = ""

while best != target_sentence:
    new_population = []
    for i in range(population_size):
        # we choose 2 parents with weights equals to scores
        parents = random.choices(population, weights=map(fitness, population), k=2)
        child = cross_over(parents[0], parents[1])
        if random.random() < mutation_rate:
            child = mutate(child)
        new_population.append(child)
    population = new_population
    iterations_count += 1
    best = "".join(max(population, key=fitness))
    print("Generation ", iterations_count, ", best so far : ", best, sep="")

print("\"", best, "\" found in generation ", iterations_count, sep="")
