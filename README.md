# Genetic sentence finder

A genetic algorithm which tries to build a given sentence from a random string.

The sentence to find is given by the variable `target_sentence`. It must only contains lowercase letters and spaces.